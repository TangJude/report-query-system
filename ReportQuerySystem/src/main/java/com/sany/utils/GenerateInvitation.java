package com.sany.utils;

import java.util.Random;

public class GenerateInvitation {

    //得到邀请码
    public static String getInvitation(){
        Random random = new Random();
        StringBuilder str = new StringBuilder("123456789ABCDEFGHIJKLMNPQRSTUVWXYZ");
        String inviteCode = "";
        for (int i = 0; i < 6; i++) {
            int index = random.nextInt(str.length());
            inviteCode+=str.charAt(index);
        }
        return inviteCode;

    }
}
