package com.sany.utils;

import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel("查询条件")
public class QueryStudentPageUtils extends PageUtils{	
	 private String sName;
	 private String phone;
}
