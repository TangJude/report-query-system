package com.sany.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("分页实体类")
public class PageUtils {
    @ApiModelProperty("页数")
    private Integer pageIndex = 1;
    @ApiModelProperty("一页有多少数量")
    private Integer pageSize = 20;

    public Integer getLimit(){
        return (pageIndex-1)*pageSize;
    }
    public Integer getOffset(){
        return pageSize;
    }
}
