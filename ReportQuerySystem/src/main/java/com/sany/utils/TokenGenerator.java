package com.sany.utils;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.connector.Request;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sany.entity.user;

@Component
public class TokenGenerator {
	
	public String getToken(HttpServletRequest req) {
		String token = req.getHeader("token");
		return token;
	}
	
	public String token(){
		StringBuffer str = new StringBuffer("qwertyuiopasdfghjklzxcvbnm123456789");
		Random random = new Random();
		String token ="";
		for (int i = 0; i < str.length(); i++) {
			int nextInt = random.nextInt(str.length());
			token+=str.charAt(nextInt);
		}
		return token;
	}
	
}
