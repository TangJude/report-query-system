package com.sany.utils;

import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@ApiModel("查询老师信息条件")
public class QueryTeacherPageUtils extends PageUtils{
	private String name;
	private String jobNumber;
}
