package com.sany.controller.backstage;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sany.entity.Day;
import com.sany.entity.Month;
import com.sany.service.backstage.BackstageSideBigDataMonitorService;
import com.sany.service.clientSide.CilenSideBigDataMonitorService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags ="后台大数据监测平台接口文档")
public class BackstageSideBigDataMonitorController {
	
	@Autowired
	private BackstageSideBigDataMonitorService service;
	@Autowired
	private TokenGenerator generator;
	
	@GetMapping("/backstageQueryEveryMonthEnrollmentQuantity/{year}")
	@ApiOperation("查询一年全部月份的招生数量接口")
	public Result<List<Month>> queryEveryMonthEnrollmentQuantity(@ApiParam("年份")@PathVariable("year") String year){
		List<Month> queryEveryMonthEnrollmentQuantity = service.queryEveryMonthAllEnrollmentQuantity(year);
		return ResultGenerator.genSuccessResult(queryEveryMonthEnrollmentQuantity);
	}
	
	@PostMapping("/backstageQueryAllEnrollmentQuantitySum")
	@ApiOperation("查询当前老师的招生总数接口")
	public Result<Integer> queryAllEnrollmentQuantitySum(){
		 Integer queryEnrollmentQuantitySum = service.queryAllEnrollmentQuantitySum();
		return ResultGenerator.genSuccessResult(queryEnrollmentQuantitySum);
	}
	
	@PostMapping("/backstageQueryTop10CityEnrollmentQuantity")
	@ApiOperation("查询全部老师招生数前10的城市接口")
	public Result<List<Map<String,Integer>>> queryTop10CityEnrollmentQuantity(){
		List<Map<String,Integer>> queryTop10CityEnrollmentQuantity = service.queryTop10CityEnrollmentQuantity();
		return ResultGenerator.genSuccessResult(queryTop10CityEnrollmentQuantity);
	}
	
	@PostMapping("/backstageQueryTop5TeacherEnrollmentQuantity")
	@ApiOperation("查询前5老师招生数量接口")
	public Result<List<Map<String,Integer>>> queryTop5TeacherEnrollmentQuantity(){
		List<Map<String,Integer>> queryTop5TeacherEnrollmentQuantity = service.queryTop5TeacherEnrollmentQuantity();
		return ResultGenerator.genSuccessResult(queryTop5TeacherEnrollmentQuantity);
	}
	
	@PostMapping("/backstageQueryLastWeekEnrollmentQuantity")
	@ApiOperation("查询上一周的招生数量")
	public Result<List<Map<String,String>>> queryLastWeekEnrollmentQuantity(){
		List<Map<String,String>> queryLastWeekEnrollmentQuantity = service.queryLastWeekEnrollmentQuantity();
		return ResultGenerator.genSuccessResult(queryLastWeekEnrollmentQuantity);
	}
	
	@PostMapping("/backstageQueryExamineeAttention")
	@ApiOperation("查询学生关注点")
	public Result<Map<String, Integer>> queryExamineeAttention(){
		Map<String, Integer> queryExamineeAttention = service.queryExamineeAttention();
		return ResultGenerator.genSuccessResult(queryExamineeAttention);
	}
}
