package com.sany.controller.backstage;

import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.service.backstage.BackstageManageStudentPageService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags ="后台管理学生接口文档")
public class BackstageManageStudentPageController {
	
	@Autowired
	private BackstageManageStudentPageService service;
	
	@PostMapping("/backstageQueryaAllStudent")
	@ApiOperation("后台查询全部学生信息")
	public Result<List<Object>> backstageQueryaAllStudent(@RequestBody PageUtils data){
		List<Object> queryAllStudent = service.queryAllStudent(data);
		return ResultGenerator.genSuccessResult(queryAllStudent);
	}
	
	@PostMapping("/backstageAddStudent")
	@ApiOperation("后台添加学生信息")
	public Result<Integer> backstageAddStudent(@RequestBody Student data){
		Integer addAndUpdateStudent = service.addAndUpdateStudent(data);
		return ResultGenerator.genSuccessResult(addAndUpdateStudent);
	}
	
	@PutMapping("/backstageUpdateStudent")
	@ApiOperation("后台修改学生信息")
	public Result<Integer> backstageUpdateStudent(@RequestBody Student data){
		Integer addAndUpdateStudent = service.addAndUpdateStudent(data);
		return ResultGenerator.genSuccessResult(addAndUpdateStudent);
	}
	
	@DeleteMapping("/backstageDeleteStudent/{sId}")
	@ApiOperation("后台删除学生信息")
	public Result<Integer> backstageDeleteStudent(@PathVariable("sId") Integer sId){
		 Integer deleteStudent = service.deleteStudent(sId);
		return ResultGenerator.genSuccessResult(deleteStudent);
	}
	
	@PostMapping("/backstageQueryStudentBy")
	@ApiOperation("后台条件查询学生")
	public Result<List<Object>> queryStudentBy(@RequestBody QueryStudentPageUtils data){
		List<Object> queryStudentBy = service.queryStudentBy(data);
		return ResultGenerator.genSuccessResult(queryStudentBy);
	}
	
	@PostMapping("/backstageImportStudnetExcel")
	@ApiOperation("控制台导入学生Excel表")
	public Result<Integer> backstageExportStudnetExcel(@RequestParam("file") MultipartFile file) throws Exception{
		ImportParams params = new ImportParams();
		//设置题目列数
		params.setTitleRows(1);
		//设置字段列数
		params.setHeadRows(2);
		List<Student> teacher = ExcelImportUtil.importExcel(file.getInputStream(),Student.class, params);
		Integer importStudentExcel = service.importStudentExcel(teacher);
		return ResultGenerator.genSuccessResult(importStudentExcel);
	}
	
	@GetMapping("/backstageExportStudnetExcel")
	@ApiOperation("控制台导出学生Excel表")
	public void backstageExportStudnetExcel(HttpServletResponse resp) throws Exception {
		List<Student> exportStudentExcel = service.exportStudentExcel();
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("学生列表信息","学生信息"), Student.class,exportStudentExcel);
		resp.setHeader("content-disposition", "attachment;fileName="+URLEncoder.encode("学生列表信息.xls","UTF-8"));
		ServletOutputStream outputStream = resp.getOutputStream();
		workbook.write(outputStream);
		outputStream.close();
		workbook.close();
	}
}
