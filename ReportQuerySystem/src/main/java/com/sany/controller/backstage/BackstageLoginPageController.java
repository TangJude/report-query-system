package com.sany.controller.backstage;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sany.entity.user;
import com.sany.service.backstage.BackstageLoginPageService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "后台登录接口文档")
public class BackstageLoginPageController {
	
	@Autowired
	private BackstageLoginPageService service;
	@Autowired
	private TokenGenerator generator;
	
	@GetMapping("/Login/{username}/{password}")
	@ApiOperation("管理员登录")
	public String Login(@ApiParam("用户名")@PathVariable(value = "username")String username,
							  @ApiParam("用户密码")@PathVariable(value = "password")String password){
		String token = service.login(username, password);
		return token;
	}
	
	@GetMapping("/queryUser")
	@ApiOperation("token获得用户信息")
	public Result<user> queryUser(HttpServletRequest req){
		String token = generator.getToken(req);
		Result<user> queryUser = service.queryUser(token);
		return queryUser;
	}
}
