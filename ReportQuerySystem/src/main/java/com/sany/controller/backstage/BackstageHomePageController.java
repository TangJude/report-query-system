package com.sany.controller.backstage;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.service.backstage.BackstageHomePageService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryTeacherPageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "后台主页信息接口文档")
public class BackstageHomePageController {
	
	@Autowired
	private BackstageHomePageService service;
	
	@PostMapping("/queryAllTeacher")
	@ApiOperation("查询所有老师信息")
	public Result queryAllTeacher(@RequestBody PageUtils data) {
		Result queryAllTeacher = service.queryAllTeacher(data);
		return queryAllTeacher;
	}
	
	@PostMapping("/addTeacher")
	@ApiOperation("添加老师信息")
	public Result<Integer> addTeacher(@RequestBody Teacher data) {
		Result<Integer> addAndUpdateTeacher = service.addAndUpdateTeacher(data);
		return addAndUpdateTeacher;
	}
	
	@GetMapping("/queryTeacherInformation/{id}")
	@ApiOperation("点击修改获得老师信息接口")
	public Result<Teacher> queryTeacherInformation(@ApiParam("教师id")@PathVariable(value = "id")Integer id){
		Result<Teacher> teacherInformation = service.queryTeacherInformation(id);
		return teacherInformation;
	}
	
	@PutMapping("/updateTeacher")
	@ApiOperation("修改老师信息")
	public Result<Integer> updateTeacher(@RequestBody Teacher data) {
		Result<Integer> addAndUpdateTeacher = service.addAndUpdateTeacher(data);
		return addAndUpdateTeacher;
	}
	
	@DeleteMapping("/deleteTeacher/{id}")
	@ApiOperation("删除老师信息")
	public Result<Integer> deleteTeacher(@ApiParam("教师id")@PathVariable(value = "id") Integer id){
		Result<Integer> deleteTeacher = service.deleteTeacher(id);
		return deleteTeacher;
	}
	
	@PostMapping("/queryTeacher")
	@ApiOperation("按条件查询老师的信息接口")
	public Result<List<Object>> queryTeacher(@RequestBody QueryTeacherPageUtils data){
		Result<List<Object>> queryTeacher = service.queryTeacher(data);
		return queryTeacher;
	}
	
	@PostMapping("/importTeacherExcel")
	@ApiOperation("导入老师Excel表")
	public Result<Integer> importExcel(@RequestParam("file") MultipartFile file) throws Exception{
		ImportParams params = new ImportParams();
		//设置题目列数
		params.setTitleRows(1);
		//设置字段列数
		params.setHeadRows(1);
		List<Teacher> teacher = ExcelImportUtil.importExcel(file.getInputStream(),Teacher.class, params);
		Integer importStudentExcel = service.importTeacherExcel(teacher);
		return ResultGenerator.genSuccessResult(importStudentExcel);
	}
	
	@GetMapping("/exportTeacherExcel")
	@ApiOperation("导出老师Excel表")
	public void exportStudentExcel(HttpServletRequest req,HttpServletResponse resp) throws Exception {
		List<Teacher> exportAllTeacher = service.exportAllTeacher();
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("老师列表信息","老师信息"), Teacher.class,exportAllTeacher);
		resp.setHeader("content-disposition", "attachment;fileName="+URLEncoder.encode("老师列表信息.xls","UTF-8"));
		ServletOutputStream outputStream = resp.getOutputStream();
		workbook.write(outputStream);
		outputStream.close();
		workbook.close();
	}
}
