package com.sany.controller.recruit;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.service.recruit.RecruitHomePageService;
import com.sany.utils.PageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.List;

@RestController
@Api(tags = "招生学生招生信息接口文档")
public class  RecruitHomePageController {

    @Autowired
    private RecruitHomePageService service;

    @Autowired
    private TokenGenerator generator;

    @PostMapping("/recruitQueryaRecruitStudent")
    @ApiOperation("招生学生招生信息")
    public Result<List<Object>> recruitQueryaRecruitStudent(@RequestBody PageUtils data, HttpServletRequest req){
        String token = generator.getToken(req);
        Result<List<Object>> listResult = service.QueryaRecruitStudent(data, token);
        return listResult;
    }

    @PostMapping("/recruitAddStudent")
    @ApiOperation("招生学生添加学生信息")
    public Result<Integer> recruitAddStudent(@RequestBody Student data,HttpServletRequest req){
        String token = generator.getToken(req);
        Result<Integer> integerResult = service.addStudent(data,token);
        return integerResult;
    }

    @GetMapping("/queryIdStudent/{id}")
    @ApiOperation("点击修改获得学生信息接口")
    public Result<Student> queryIdRecruit(@ApiParam("招生学生id")@PathVariable(value = "id")Integer id){
        Result<Student> studentResult = service.queryIdStudent(id);
        return studentResult;
    }

    @PutMapping("/recruitUpdateStudent")
    @ApiOperation("修改学生信息接口")
    public Result<Integer> recruitUpdateStudent(@RequestBody Student student){
        Integer integer = service.updateStudent(student);
        return ResultGenerator.genSuccessResult(integer);
    }


    //删除学生信息
    @DeleteMapping("recruitDeleteStudent/{sid}")
    @ApiOperation("删除学生信息")
    public Result<Integer> recruitDeleteStudent(@ApiParam("学生id")@PathVariable("sid") int sid){
        Integer integer = service.recruitDelete(sid);
        return ResultGenerator.genSuccessResult(integer);
    }

    @GetMapping("recruitExportRecruitStudent")
    @ApiOperation("导出招生学生招的学生的信息")
    public void recruitExportRecruitStudent(HttpServletResponse resp,@RequestParam("token")String token) throws Exception {
        List<Student> students = service.exportRecruitStudentExcel(token);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("学生列表信息","学生信息"), Student.class,students);
        resp.setHeader("content-disposition", "attachment;fileName="+ URLEncoder.encode("学生列表.xls","UTF-8"));
        ServletOutputStream outputStream = resp.getOutputStream();
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    @PostMapping("/recruitImportStudnetExcel")
    @ApiOperation("招生学生导入学生Excel表")
    public Result<Integer> recruitImportStudnetExcel(@RequestParam("file") MultipartFile file,HttpServletRequest req) throws Exception{
        ImportParams params = new ImportParams();
        //设置题目列数
        params.setTitleRows(1);
        //设置字段列数
        params.setHeadRows(2);
        List<Student> teacher = ExcelImportUtil.importExcel(file.getInputStream(),Student.class, params);
        String token = generator.getToken(req);
        Integer importStudentExcel = service.importRecruitStudentExcel(teacher,token);
        return ResultGenerator.genSuccessResult(importStudentExcel);
    }
}
