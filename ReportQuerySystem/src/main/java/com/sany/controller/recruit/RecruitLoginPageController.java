package com.sany.controller.recruit;

import com.sany.entity.Recruit;
import com.sany.entity.Teacher;
import com.sany.service.recruit.RecruitLoginPageService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(tags ="招生学生登录接口文档")
public class RecruitLoginPageController {

    @Autowired
    RecruitLoginPageService service;

    @Autowired
    TokenGenerator generator;

    @GetMapping("/recruitLogin/{invitationCode}/{password}")
    @ApiOperation("邀请码登录接口")
    public Result recruitLogin(@ApiParam("邀请码")@PathVariable("invitationCode")String invitationCode,
                               @ApiParam("密码")@PathVariable("password")String password){
        return service.Login(invitationCode, password);
    }

    @GetMapping("/tokenQueryRecruit")
    @ApiOperation("token查询招生学生信息")
    public Result<Teacher> tokenQueryTeacher(HttpServletRequest req){
        String token = generator.getToken(req);
        Recruit recruit = service.queryRecruit(token);
        return ResultGenerator.genSuccessResult(recruit);
    }




}
