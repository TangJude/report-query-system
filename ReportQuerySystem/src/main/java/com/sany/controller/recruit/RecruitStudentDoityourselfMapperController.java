package com.sany.controller.recruit;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.sany.entity.Student;
import com.sany.entity.newobject;
import com.sany.service.recruit.RecruitInformationPageService;
import com.sany.service.recruit.RecruitStudentDoityourselfService;
import com.sany.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@RestController
@Api(tags = "关于我还没弄清楚这件事")
public class RecruitStudentDoityourselfMapperController {
    @Autowired
    private RecruitInformationPageService service;

    @Autowired
    private TokenGenerator tokenGenerator;
    @Autowired
    private RecruitStudentDoityourselfService recruitStudentDo_it_yourselfMapper;

    @GetMapping("/exportTeacherExcelToOut")
    @ApiOperation("导出老师的招生学生的招生信息Excel表")
    public void exportStudentExcel(@RequestParam("token")String token,HttpServletResponse resp) throws Exception {
        List<Student> exportAllTeacher = recruitStudentDo_it_yourselfMapper.QuerytoOut(token);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("学生列表信息","学生信息"), Student.class,exportAllTeacher);

        resp.setHeader("content-disposition", "attachment;fileName="+URLEncoder.encode("学生列表信息.xls","UTF-8"));
        ServletOutputStream outputStream = resp.getOutputStream();
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    @PostMapping("/selectAllInternalStudentsPart")
    @ApiOperation("内部学生查询自己招的学生")
    public newobject selectAllInternalStudentsPart (HttpServletRequest req,@RequestBody QueryStudentPageUtils data){
        String token = tokenGenerator.getToken(req);
        newobject newobject = recruitStudentDo_it_yourselfMapper.selectAllInternalStudentsPart(token,data);
        return newobject;
    }


    //老师查询内部学生招的外部学生
    @PostMapping("/teacherqueryexternal")
    @ApiOperation("老师查询内部学生招的外部学生")
    public newobject teacherqueryexternal(HttpServletRequest req,@RequestBody QueryStudentPageUtils data){
        String token = tokenGenerator.getToken(req);
        newobject newobject1 = recruitStudentDo_it_yourselfMapper.teacherqueryexternal(token,data);
        return newobject1;
    }

    @PostMapping("/teacherquerypartexternal")
    @ApiOperation("老师查询内部学生")
    public newobject teacherquerypartexternal(HttpServletRequest req,@RequestBody QueryStudentPageUtils data){
        String token = tokenGenerator.getToken(req);
        newobject newobject1 = recruitStudentDo_it_yourselfMapper.teacherquerypartexternal(token,data);
        return newobject1;
    }



}
