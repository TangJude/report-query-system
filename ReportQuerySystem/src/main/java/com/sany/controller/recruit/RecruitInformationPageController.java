package com.sany.controller.recruit;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.service.recruit.RecruitInformationPageService;
import com.sany.utils.PageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

@RestController
@Api(tags = "招生信息接口文档")
public class RecruitInformationPageController {

    @Autowired
    private RecruitInformationPageService service;
    @Autowired
    private TokenGenerator generator;

    @PostMapping("/recruitQueryaAllRecruit")
    @ApiOperation("老师查询全部学生招生学生信息")
    public Result<List<Object>> recruitQueryAllStudent(@RequestBody PageUtils data, HttpServletRequest req){
        String token = generator.getToken(req);
        List<Object> queryAllRecruitVo = service.queryAllRecruitVo(data,token);
        return ResultGenerator.genSuccessResult(queryAllRecruitVo);
    }

    @PostMapping("/recruitQueryTeacherInvitation")
    @ApiOperation("老师查询全部邀请码信息")
    public Result<List<Object>> recruitQueryTeacherInvitation(@RequestBody PageUtils data,HttpServletRequest req){
        String token = generator.getToken(req);
        Result<List<Object>> listResult = service.queryTeacherInvitation(data,token);
        return listResult;
    }

    @PostMapping("/randomInformation")
    @ApiOperation("得到随机邀请码")
    public String recruitRandomInformation(HttpServletRequest req){
        String token = generator.getToken(req);
        return service.randomInformation(token);
    }

    @PostMapping("/addInformationCode")
    @ApiOperation("生成邀请码")
    public Result recruitAddInformationCode(@RequestBody Recruit recruit, HttpServletRequest req){
        String token = generator.getToken(req);
        Result result = service.addInformation(token, recruit);
        return result;
    }

    @DeleteMapping("/deleteInformation/{rid}")
    @ApiOperation("删除邀请码")
    public Result deleteInformation(@ApiParam("邀请码id")@PathVariable("rid") int rid){
        Result<Integer> integerResult = service.deleteInvitation(rid);
        return integerResult;
    }

    @GetMapping("/queryIdRecruit/{id}")
    @ApiOperation("点击修改获得招生学生信息接口")
    public Result<Recruit> queryIdRecruit(@ApiParam("招生学生id")@PathVariable(value = "id")Integer id){
        Result<Recruit> teacherInformation = service.queryIdRecruit(id);
        return teacherInformation;
    }

    @PostMapping("updateRecruit")
    @ApiOperation("修改招生学生信息接口")
    public Result updeteleRecruit(@RequestBody Recruit recruit){
        Result result = service.updateRecruit(recruit);
        return result;
    }

    @GetMapping("/recruitExportRecruit")
    @ApiOperation("导出老师名下招生学生信息")
    public void recruitExportRecruit(HttpServletResponse resp, @RequestParam("jobNumber") String jobNumber) throws Exception {
        List<Recruit> recruits = service.exportRecruitExcel(jobNumber);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("学生列表信息", "学生信息"), Recruit.class, recruits);
        resp.setHeader("content-disposition","attachment;fileName="+ URLEncoder.encode("学生列表.xls","UTF-8"));
        ServletOutputStream outputStream = resp.getOutputStream();
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    @PostMapping("/recruitImportResultExcel")
    @ApiOperation("导入老师名下招生学生信息")
    public Result<Integer> recruitImportStudnetExcel(@RequestParam("file") MultipartFile file, HttpServletRequest req) throws Exception {
        ImportParams params = new ImportParams();
        //设置题目列数
        params.setTitleRows(1);
        //设置字段列数
        params.setHeadRows(2);
        List<Recruit> recruits = ExcelImportUtil.importExcel(file.getInputStream(),Recruit.class, params);
        String token = generator.getToken(req);
        Integer integer = service.importRecruitExcel(recruits, token);
        return ResultGenerator.genSuccessResult(integer);
    }


}
