package com.sany.controller.clientSide;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sany.entity.Day;
import com.sany.entity.Month;
import com.sany.service.clientSide.CilenSideBigDataMonitorService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags ="客户端大数据监测平台接口文档")
public class CilenSideBigDataMonitorController {
	
	@Autowired
	private CilenSideBigDataMonitorService service;
	@Autowired
	private TokenGenerator generator;
	
	@GetMapping("/queryEveryMonthEnrollmentQuantity/{year}")
	@ApiOperation("查询一年全部月份的招生数量接口")
	public Result<List<Month>> queryEveryMonthEnrollmentQuantity(@ApiParam("年份")@PathVariable("year") String year,HttpServletRequest req){
		String token = generator.getToken(req);
		List<Month> queryEveryMonthEnrollmentQuantity = service.queryEveryMonthEnrollmentQuantity(year, token);
		return ResultGenerator.genSuccessResult(queryEveryMonthEnrollmentQuantity);
	}
	
	@PostMapping("/queryEnrollmentQuantitySum")
	@ApiOperation("查询当前老师的招生总数接口")
	public Result<Integer> queryEnrollmentQuantitySum(HttpServletRequest req){
		String token = generator.getToken(req);
		 Integer queryEnrollmentQuantitySum = service.queryEnrollmentQuantitySum(token);
		return ResultGenerator.genSuccessResult(queryEnrollmentQuantitySum);
	}
	
	@PostMapping("/queryTop5CityEnrollmentQuantity")
	@ApiOperation("查询当前老师招生数前5的城市接口")
	public Result<List<Map<String,Integer>>> queryTop5CityEnrollmentQuantity(HttpServletRequest req){
		String token = generator.getToken(req);
		List<Map<String,Integer>> queryTop5CityEnrollmentQuantity = service.queryTop5CityEnrollmentQuantity(token);
		return ResultGenerator.genSuccessResult(queryTop5CityEnrollmentQuantity);
	}
	
	@PostMapping("/queryDateEnrollmentQuantity")
	@ApiOperation("查询当前包含今天的前7天招生数量接口")
	public Result<List<Day>> queryDateEnrollmentQuantity(HttpServletRequest req){
		String token = generator.getToken(req);
		List<Day> queryDateEnrollmentQuantity = service.queryDateEnrollmentQuantity(token);
		return ResultGenerator.genSuccessResult(queryDateEnrollmentQuantity);
	}
	
	@PostMapping("/queryLastWeekEnrollmentQuantity")
	@ApiOperation("查询上一周的招生数量")
	public Result<List<Map<String,String>>> queryLastWeekEnrollmentQuantity(HttpServletRequest req){
		String token = generator.getToken(req);
		List<Map<String,String>> queryLastWeekEnrollmentQuantity = service.queryLastWeekEnrollmentQuantity(token);
		return ResultGenerator.genSuccessResult(queryLastWeekEnrollmentQuantity);
	}
	
	@PostMapping("/queryExamineeAttention")
	@ApiOperation("查询学生关注点")
	public Result<Map<String, Integer>> queryExamineeAttention(HttpServletRequest req){
		String token = generator.getToken(req);
		Map<String, Integer> queryExamineeAttention = service.queryExamineeAttention(token);
		return ResultGenerator.genSuccessResult(queryExamineeAttention);
	}
}
