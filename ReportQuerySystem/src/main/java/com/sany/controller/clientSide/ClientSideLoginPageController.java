package com.sany.controller.clientSide;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sany.entity.Teacher;
import com.sany.service.clientSide.ClientSideLoginPageService;
import com.sany.utils.PageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags ="客户端登录接口文档")
public class ClientSideLoginPageController {
	
	@Autowired
	private ClientSideLoginPageService service;
	@Autowired
	private TokenGenerator generator;
	
	@GetMapping("/teacherLogin/{jobNumber}/{password}")
	@ApiOperation("教师登录接口")
	public String teacherLogin(@ApiParam("教师工号")@PathVariable("jobNumber")String jobNumber,
										@ApiParam("密码")@PathVariable("password")String password){
		return service.Login(jobNumber, password);
	}
	
	@GetMapping("/tokenQueryTeacher")
	@ApiOperation("token查询老师信息")
	public Result<Teacher> tokenQueryTeacher(HttpServletRequest req){
			String token = generator.getToken(req);
			Teacher queryTeacher = service.queryTeacher(token);
			return ResultGenerator.genSuccessResult(queryTeacher);
	}
}
