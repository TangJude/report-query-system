package com.sany.controller.clientSide;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sany.entity.Student;
import com.sany.service.clientSide.ClientSideHomePageService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags ="客户端主页接口文档")
public class ClientSideHomePageController {
	
	@Autowired
	private ClientSideHomePageService service;
	@Autowired
	private TokenGenerator generator;
	
	@PostMapping("/queryAllStudent")
	@ApiOperation("查询全部学生信息接口")
	public Result<ArrayList<Object>> queryAllStudent(@RequestBody PageUtils data,HttpServletRequest req){
		String token = generator.getToken(req);
		Result<ArrayList<Object>> queryAllStudent = service.queryAllStudent(data,token);
		return queryAllStudent;
	}
	
	@PostMapping("/addStudent")
	@ApiOperation("添加学生信息")
	public Result<Integer> addStudent(@RequestBody Student data,HttpServletRequest req){
		String token = generator.getToken(req);
		Result<Integer> addAndUpdateStudent = service.addAndUpdateStudent(data,token);
		return addAndUpdateStudent;
	}
	
	@GetMapping("/queryUpdateStudent/{sId}")
	@ApiOperation("查询要修改的学生信息")
	public Result<Student> queryUpdateStudent(@PathVariable("sId")Integer sId){
		Result<Student> queryUpdateStudent = service.queryUpdateStudent(sId);
		return queryUpdateStudent;
	}
	
	@PutMapping("/updateStudent")
	@ApiOperation("修改学生信息")
	public Result<Integer> updateStudent(@RequestBody Student data,HttpServletRequest req){
		String token = generator.getToken(req);
		Result<Integer> addAndUpdateStudent = service.addAndUpdateStudent(data,token);
		return addAndUpdateStudent;
	}
	
	@DeleteMapping("/deleteStudent/{sId}")
	@ApiOperation("删除学生信息")
	public Result<Integer> deleteStudent(@PathVariable("sId")Integer sId,HttpServletRequest req){
		String token = generator.getToken(req);
		Result<Integer> deleteStudent = service.deleteStudent(sId,token);
		return deleteStudent;
	}
	
	@PostMapping("/queryStudnetInformation")
	@ApiOperation("按条件查询学生信息")
	public Result<List<Object>> queryStudnetInformation(@RequestBody QueryStudentPageUtils data,HttpServletRequest req){
		String token = generator.getToken(req);
		 Result<List<Object>> queryStudnetInformation = service.queryStudnetInformation(data,token);
		return queryStudnetInformation;
	}
	
	@PostMapping("/importStudentExcel")
	@ApiOperation("导入学生Excel表")
	public Result<Integer> importExcel(@RequestParam("file") MultipartFile file) throws Exception{
		ImportParams params = new ImportParams();
		//设置题目列数
		params.setTitleRows(1);
		//设置字段列数
		params.setHeadRows(2);
		List<Student> studens = ExcelImportUtil.importExcel(file.getInputStream(),Student.class, params);
		Integer importStudentExcel = service.importStudentExcel(studens);
		return ResultGenerator.genSuccessResult(importStudentExcel);
	}
	
	@GetMapping("/exportStudentExcel")
	@ApiOperation("导出学生Excel表")
	public void exportStudentExcel(HttpServletRequest req,HttpServletResponse resp,@RequestParam("jobNumber")String jobNumber) throws Exception {
		List<Student> students = service.queryTeacherRecruitAllStudent(jobNumber);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("学生列表信息","学生信息"), Student.class,students);
		resp.setHeader("content-disposition", "attachment;fileName="+URLEncoder.encode("学生列表.xls","UTF-8"));
		ServletOutputStream outputStream = resp.getOutputStream();
		workbook.write(outputStream);
		outputStream.close();
		workbook.close();
	}
	
} 