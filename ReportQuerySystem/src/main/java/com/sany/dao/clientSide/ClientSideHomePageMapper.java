package com.sany.dao.clientSide;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;

@Mapper
public interface ClientSideHomePageMapper {
	//查询单个老师招的全部学生
	List<Student> queryAllStudent(PageUtils data,@Param("token") String token);
	//查询单个老师招的全部学生总数
	Integer queryAllStudentCount(@Param("token") String token);
	
	//查询要修改的学生信息
	Student queryUpdateStudent(@Param("sId")Integer sId);
	
	//修改学生信息
	Integer updateStudent(Student data);
	
	//条件查询
	List<Student> queryStudnetInformation(QueryStudentPageUtils data,String token);
	Integer queryStudentCount(QueryStudentPageUtils data,String token);
	
	Teacher queryTeacher(@Param("token") String token);
	
	
	//查询招生数量从大到小的老师
	List<Teacher> queryMaxEnrollmentQuantity();
	
	List<Student> queryTeacherRecruitAllStudent(@Param("jobNumber") String jobNumber);
	
	Integer addStudents(Student data);
	
	//添加学生信息
	Integer addStudent(Student data);
	//增加老师的招生数量
	Integer addEnrollmentQuantity(@Param("jobNumber")String jobNumber);
	
	//删除学生信息
	Integer deleteStudent(@Param("sId")Integer sId);
	//减少老师招生数量
	Integer reduceEnrollmentQuantity(@Param("jobNumber")String jobNumber);
	
	//用手机号查询
	Student queryStudentByIdentityCard(@Param("phone")String phone);
	
	//用手机号修改学生信息
	Integer updateStudentByIdentityCard(Student data);
	
	//刷新排名
	Integer updateTeacherRank(@Param("rank")Integer rank,@Param("jobNumber")String jobNumber);

}
