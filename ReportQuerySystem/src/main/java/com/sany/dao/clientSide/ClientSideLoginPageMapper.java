package com.sany.dao.clientSide;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.Teacher;

@Mapper
public interface ClientSideLoginPageMapper {
	
	String queryTeacherToken(@Param("jobNumber")String jobNumber,@Param("password")String password);
	
	Teacher queryTeacher(@Param("token")String token);
}
