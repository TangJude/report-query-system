package com.sany.dao.clientSide;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.Day;
import com.sany.entity.Month;

@Mapper
public interface CilenSideBigDataMonitorMapper {
	//我的月份招生数量趋势
	List<Month> queryEveryMonthEnrollmentQuantity(@Param("year") String year,@Param("token")String token);
	
	//我的招生数量总数
	Integer queryEnrollmentQuantitySum(@Param("token")String token);
	
	//前5座城市招生数量
	List<Map<String,Integer>> queryTop5CityEnrollmentQuantity(@Param("token")String token); 
	
	//查询当前包含今天的前7天招生数量
	List<Day> queryDateEnrollmentQuantity(@Param("token")String token);
	
	//查询上一周的招生数量
	List<Map<String,String>> queryLastWeekEnrollmentQuantity(@Param("token")String token);
	
	//查询考生关注点
	List<String> queryExamineeAttention(@Param("token")String token);
}
