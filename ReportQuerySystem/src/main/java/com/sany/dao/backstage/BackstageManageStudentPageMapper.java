package com.sany.dao.backstage;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;

@Mapper
public interface BackstageManageStudentPageMapper {
	
	//查询全部学生数据
	List<Student> queryAllStudnet(PageUtils date);

	//查询全部学生总数
	Integer queryAllStudentCount();
	
	Integer addStudents(Student data);
	
	Integer updateStudent(Student data);
	
	Integer deleteStudent(@Param("sId")Integer sId);
	
	//查询招生数量从大到小的老师
	List<Teacher> queryMaxEnrollmentQuantity();
	
	Integer reduceEnrollmentQuantity(@Param("jobNumber")String jobNumber);
	
	String queryTeacherJobNumber(@Param("sId")Integer sId);
	
	//增加招生老师的招生数量
	Integer addEnrollmentQuantity(@Param("jobNumber")String jobNumber);
	
	Integer updateTeacherRank(@Param("rank")Integer rank,@Param("jobNumber")String jobNumber);
	
	//用手机号查询student
	Student queryStudentByIdentityCard(@Param("phone")String phone);
	
	//用身份证修改学生信息
	Integer updateStudentByIdentityCard(Student data);
	
	//条件查询
	List<Student> queryStudentBy(QueryStudentPageUtils data);
	Integer queryStudentCount(QueryStudentPageUtils data);
	
	List<Student> queryImportStudent();
}
