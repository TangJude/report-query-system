package com.sany.dao.backstage;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.Day;
import com.sany.entity.Month;

@Mapper
public interface BackstageSideBigDataMonitorMapper {
	//月份全部招生数量趋势
	List<Month> queryEveryMonthAllEnrollmentQuantity(@Param("year") String year);
	
	//全部招生数量总数
	Integer queryAllEnrollmentQuantitySum();
	
	//前10座城市招生数量
	List<Map<String,Integer>> queryTop10CityEnrollmentQuantity(); 
	
	//查询前5招生数量的老师
	List<Map<String,Integer>> queryTop5TeacherEnrollmentQuantity();
	
	//查询上一周的招生数量
	List<Map<String,String>> queryLastWeekEnrollmentQuantity();
	
	//查询考生关注点
	List<String> queryExamineeAttention();
}
