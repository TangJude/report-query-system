package com.sany.dao.backstage;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryTeacherPageUtils;

@Mapper
public interface BackstageHomePageMapper {
	//查询所有老师信息
	List<Teacher> queryAllTeacher(PageUtils data);
	
	//查询所有老师数量
	Integer queryAllTeacherCount();
	
	//添加老师信息
	Integer addTeacher(Teacher data);
	
	//修改老师信息
	Integer updateTeacher(Teacher data);
	
	Integer updateTeacherByjobNumber(Teacher data);
	
	//点击编辑时获取的信息
	Teacher queryTeacherInformation(@Param("id") Integer id);
	
	//查询老师信息
	List<Teacher> queryTeacher(QueryTeacherPageUtils data);
	Integer queryTeacherCount(QueryTeacherPageUtils data);
	
	Teacher queryRankMinimumTeacher();
	
	Integer queryRankMinimum();
	
	//创建老师的时候添加token
	Integer addTeacherToken(@Param("username")String username,@Param("token")String token);
	//删除老师的时候去掉token
	Integer deleteTeacherToken(@Param("username")String username);
	
	List<Teacher> queryExportAllTeacher();
	
	//删除老师信息
	Integer deleteTeacher(@Param("id") int id);
	//查询招生数量从大到小的老师
	List<Teacher> queryMaxEnrollmentQuantity();
	
	Integer updateTeacherRank(@Param("rank")Integer rank,@Param("jobNumber")String jobNumber);
	
	//查询工号是否存在
	Teacher queryTeacherByJobNumber(@Param("jobNumber")String jobNumber);
	}
