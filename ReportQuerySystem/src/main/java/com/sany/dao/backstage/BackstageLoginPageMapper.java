package com.sany.dao.backstage;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sany.entity.user;


@Mapper
public interface BackstageLoginPageMapper {
	
	//用户登录
	String queryUserToken(@Param("username")String username,@Param("password")String password);
	
	//token查询用户
	user queryUser(String token);
	
	Integer addEnrollmentQuantity(@Param("jobNumber")String jobNumber);
	
	Integer reduceEnrollmentQuantity(@Param("jobNumber")String jobNumber);
	
	Integer updateTeacherRank(@Param("rank") Integer rank,@Param("id") Integer id);
}
