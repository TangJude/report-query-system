package com.sany.dao.recruit;

import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.selectObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RecruitStudentDoityourselfMapper {
    List<Student> selectAllStudent(String token);

    //内部学生查询外部学生
    List<Student> selectAllInternalStudentsPart(@Param("token") String token, QueryStudentPageUtils data);
    Integer selectAllInternalStudentsPartNumber(@Param("token") String token, QueryStudentPageUtils data);

    //    老师查询自己名下内部学生招的学生
    List<Student> teacherqueryexternal(@Param("token")String token,QueryStudentPageUtils data);
    Integer teacherqueryexternalNumber(@Param("token")String token,QueryStudentPageUtils data);

    //    老师查询自己名下的全部内部学生的
    List<Recruit> teacherquerypartexternal(@Param("token")String token,QueryStudentPageUtils data);
    Integer teacherquerypartexternalNumber(@Param("token")String token,QueryStudentPageUtils data);



}
