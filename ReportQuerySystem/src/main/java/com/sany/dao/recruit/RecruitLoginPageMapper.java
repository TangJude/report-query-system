package com.sany.dao.recruit;

import com.sany.entity.Recruit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RecruitLoginPageMapper {

    //招生学生登录
      String queryRecruitToken(@Param("invitationCode") String invitationCode,@Param("password")String password);

      //根据token查询老师信息
    Recruit queryRecruit(@Param("token") String token);


}
