package com.sany.dao.recruit;

import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RecruitInformationPageMapper {

    //查询全部招生学生招生数据
    List<Student> queryAllRecruit(@Param("jobNumber")String jobNumber, PageUtils data);

    //查询全部招生学生总数
    Integer queryAllRecruitCount(@Param("jobNumber")String jobNumber);

    //根据token查询老师信息
    Teacher queryTeacher(@Param("token")String token);

    //根据老师工号查询邀请码信息
    List<Recruit> queryTeacherInformation(@Param("jobNumber")String jobNumber,PageUtils data);
    List<Recruit> queryjobNumber(@Param("jobNumber")String jobNumber);

    //查询老师名下的邀请码数量
    Integer queryTeacherInformationCount(@Param("jobNumber")String jobNumber);

    //添加邀请码信息
    Integer addInformation(Recruit recruit);

    //添加token信息
    Integer addInformationToken(@Param("information") String information,@Param("token") String token);

    //查询邀请码是否已存在
    Recruit queryInformation(@Param("information") String information);

    //根据id查询邀请码信息
    Recruit queryIdInformation(@Param("rid") Integer rid);

    //删除邀请码
    Integer deleteInformation(@Param("rid") int rid);

    //删除token
    Integer deleteInformationToken(@Param("information") String information);

    //修改招生学生招生信息
    Integer updateRecruit(Recruit recruit);

}
