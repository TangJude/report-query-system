package com.sany.dao.recruit;

import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RecruitHomePageMapper {

    //根据token查询邀请码
    String queryInformation(@Param("token") String token);

    //根据邀请码查询出学生信息
    List<Student> queryStudent(PageUtils data, @Param("invitationCode") String invitationCode);

    List<Student> queryRecruitStudent(@Param("invitationCode") String invitationCode);

    //根据邀请码查询出学生数量
    Integer queryStudentCount(@Param("invitationCode")String invitationCode);

    //根据邀请码查询招生学生信息
    Recruit queryRecruit(@Param("invitationCode") String invitationCode);

    //根据教师工号查询教师信息
    Teacher queryTeacher(@Param("jobNumber") String jobNumber);

    //招生学生招生数量加一
    Integer RecruitEnrollmentQuantity(@Param("invitationCode") String invitationCode);

    //老师招生数量加一
    Integer TeacherEnrollmentQuantity(@Param("jobNumber") String jobNumber);

    //添加学生
    Integer addStudent(Student student);

    //根据招生数量输出老师排名
    List<Teacher> querySortTeacher();

    //更改教师排名
    Integer updateTeacherRank(@Param("rank")Integer rank,@Param("jobNumber")String jobNumber);

    //老师招生数量减1
    Integer TeacherDeleteEnrollment(@Param("jobNumber") String jobNumber);

    //招生学生数量减1
    Integer RecruitDeleteEnrollment(@Param("invitationCode") String invitationCode);

    //根据id查询学生信息
    Student queryIdStudent(@Param("sid") Integer sid);

    Integer deleteStudent(@Param("sid") Integer sid);

    Integer updateStudent(Student student);

    //根据手机号查询学生信息
    Student queryStudentByIdentityCard(@Param("phone") String phone);

    //根据手机号修改学生信息
    Integer updateStudentByIdentityCard(Student student);

}
