package com.sany.service.backstage;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.sany.entity.Day;
import com.sany.entity.Month;

public interface BackstageSideBigDataMonitorService {
	//月份全部招生数量趋势
	List<Month> queryEveryMonthAllEnrollmentQuantity(String year);
	
	//全部招生数量总数
	Integer queryAllEnrollmentQuantitySum();
	
	//前5座城市招生数量
	List<Map<String,Integer>> queryTop10CityEnrollmentQuantity(); 
	
	//查询当前包含今天的前7天招生数量
	List<Map<String,Integer>> queryTop5TeacherEnrollmentQuantity();
	
	//查询上一周的招生数量
	List<Map<String,String>> queryLastWeekEnrollmentQuantity();
	
	
	//查询考生关注点
	Map<String,Integer> queryExamineeAttention();
}
