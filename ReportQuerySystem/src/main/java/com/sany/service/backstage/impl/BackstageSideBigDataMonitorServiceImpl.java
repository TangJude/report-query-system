package com.sany.service.backstage.impl;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sany.dao.backstage.BackstageSideBigDataMonitorMapper;
import com.sany.dao.clientSide.CilenSideBigDataMonitorMapper;
import com.sany.entity.Day;
import com.sany.entity.Month;
import com.sany.service.backstage.BackstageSideBigDataMonitorService;
import com.sany.service.clientSide.CilenSideBigDataMonitorService;
import com.sany.utils.DateUtils;


@Service
public class BackstageSideBigDataMonitorServiceImpl implements BackstageSideBigDataMonitorService {
	
	@Autowired
	private BackstageSideBigDataMonitorMapper mapper;
	@Autowired
	private DateUtils dateUtils;
	
	@Override
	public List<Month> queryEveryMonthAllEnrollmentQuantity(String year) {
		// TODO Auto-generated method stub
		List<Month> queryEveryMonthEnrollmentQuantity = mapper.queryEveryMonthAllEnrollmentQuantity(year);
		return queryEveryMonthEnrollmentQuantity;
	}

	@Override
	public Integer queryAllEnrollmentQuantitySum() {
		// TODO Auto-generated method stub
		Integer queryEnrollmentQuantitySum = mapper.queryAllEnrollmentQuantitySum();
		return queryEnrollmentQuantitySum;
	}

	@Override
	public List<Map<String,Integer>> queryTop10CityEnrollmentQuantity() {
		// TODO Auto-generated method stub
		List<Map<String,Integer>> queryTop10CityEnrollmentQuantity = mapper.queryTop10CityEnrollmentQuantity();
		return queryTop10CityEnrollmentQuantity;
	}

	@Override
	public List<Map<String,Integer>> queryTop5TeacherEnrollmentQuantity() {
		// TODO Auto-generated method stub
		 List<Map<String,Integer>> queryTop5TeacherEnrollmentQuantity = mapper.queryTop5TeacherEnrollmentQuantity();
		return queryTop5TeacherEnrollmentQuantity;
	}

	@Override
	public List<Map<String,String>> queryLastWeekEnrollmentQuantity() {
		// TODO Auto-generated method stub
		List<Map<String,String>> queryLastWeekEnrollmentQuantity = mapper.queryLastWeekEnrollmentQuantity();
		queryLastWeekEnrollmentQuantity.forEach(time->{
			 String date = time.get("month");
			 Date stringSwitchDate = dateUtils.StringSwitchDate(date);
			 Integer week = dateUtils.getWeek(stringSwitchDate);
			 time.put("month", week+"");
		});
		return queryLastWeekEnrollmentQuantity;
	}
	
	public Map<String,Integer> queryExamineeAttention() {
		List<String> data = mapper.queryExamineeAttention();
		Map<String,Integer> hashMap = new HashMap<>();
		int tuition=0;int employment=0;int major=0;
		int ambient=0;int canteen=0;int dormitory=0;
		for (int i = 0; i < data.size(); i++) {
			String[] split = data.get(i).split("/");
			for (int j = 0; j < split.length; j++) {
				if("学费".equals(split[j])) {tuition++;}
				if("就业".equals(split[j])) {employment++;}
				if("专业".equals(split[j])) {major++;}
				if("环境".equals(split[j])) {ambient++;}
				if("食堂".equals(split[j])) {canteen++;}
				if("宿舍".equals(split[j])) {dormitory++;}
			}
		}
		hashMap.put("tuition", tuition);hashMap.put("employment", employment);
		hashMap.put("major", major);hashMap.put("ambient", ambient);
		hashMap.put("canteen", canteen);hashMap.put("dormitory", dormitory);
		return hashMap;
	}
}
