package com.sany.service.backstage;

import java.util.ArrayList;
import java.util.List;

import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryTeacherPageUtils;
import com.sany.utils.Result;

public interface BackstageHomePageService {
	
	Result<ArrayList<Object>> queryAllTeacher(PageUtils data);
	
	Result<Integer> addAndUpdateTeacher(Teacher data);
	
	Result<Integer> deleteTeacher(int id);
	
	Result<Teacher> queryTeacherInformation(Integer id);
	
	Result<List<Object>> queryTeacher(QueryTeacherPageUtils data);
	
	Integer importTeacherExcel(List<Teacher> teacher);
	
	List<Teacher> exportAllTeacher();
}
