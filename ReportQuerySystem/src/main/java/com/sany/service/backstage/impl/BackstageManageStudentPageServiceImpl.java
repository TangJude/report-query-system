package com.sany.service.backstage.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sany.dao.backstage.BackstageManageStudentPageMapper;
import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.service.backstage.BackstageManageStudentPageService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;

@Service
@Transactional
public class BackstageManageStudentPageServiceImpl implements BackstageManageStudentPageService{
	
	@Autowired
	private BackstageManageStudentPageMapper mapper;
	
	@Override
	public List<Object> queryAllStudent(PageUtils data) {
		// TODO Auto-generated method stub
		List<Object> lists = new ArrayList<>();
		List<Student> queryAllStudnet = mapper.queryAllStudnet(data);
		Integer queryAllStudentCount = mapper.queryAllStudentCount();
		lists.add(queryAllStudnet);
		lists.add(queryAllStudentCount);
		return lists;
	}

	@Override
	public Integer addAndUpdateStudent(Student data) {
		// TODO Auto-generated method stub
		Integer i =0;
		Student queryStudentByIdentityCard = mapper.queryStudentByIdentityCard(data.getPhone());
			if(data.getSId()==0) {
				if(queryStudentByIdentityCard==null) {
				i = mapper.addStudents(data);
				Integer addEnrollmentQuantity = mapper.addEnrollmentQuantity(data.getJobNumber());
				if(addEnrollmentQuantity==1) {
					int rank=1;
					List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
					for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
						mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
						if(j+1!=queryMaxEnrollmentQuantity.size()) {
							if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
								rank++;
							}
						}
					}
				}
				}
			}else {
				if(queryStudentByIdentityCard==null||queryStudentByIdentityCard.getSId()==data.getSId()) {
					i = mapper.updateStudent(data);
				}
			}
		
		return i;
	}

	@Override
	public Integer deleteStudent(Integer sId) {
		// TODO Auto-generated method stub
			String jobNumber = mapper.queryTeacherJobNumber(sId);
			Integer reduceEnrollmentQuantity = mapper.reduceEnrollmentQuantity(jobNumber);
			if(reduceEnrollmentQuantity!=0 || reduceEnrollmentQuantity!=null) {
				int rank=1;
				List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
				for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
					mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
					if(j+1!=queryMaxEnrollmentQuantity.size()) {
						if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
							rank++;
						}
					}
				}
			}
			Integer deleteStudent = mapper.deleteStudent(sId);
		return deleteStudent;
	}

	@Override
	public List<Student> exportStudentExcel() {
		// TODO Auto-generated method stub
		return  mapper.queryImportStudent();
	}

	@Override
	public Integer importStudentExcel(List<Student> data) {
		// TODO Auto-generated method stub
			data.forEach(time->{
			if(time.getSName()!=null) {
				Student queryStudentByIdentityCard = mapper.queryStudentByIdentityCard(time.getPhone());
				if(queryStudentByIdentityCard==null) {
				mapper.addStudents(time);
					Integer addEnrollmentQuantity = mapper.addEnrollmentQuantity(time.getJobNumber());
				}else {
				mapper.updateStudentByIdentityCard(time);
				}
			}
		});
		int rank=1;
		List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
		for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
			mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
			if(j+1!=queryMaxEnrollmentQuantity.size()) {
				if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
					rank++;
				}
			}
		}
		return 1;
	}

	@Override
	public List<Object> queryStudentBy(QueryStudentPageUtils data) {
		// TODO Auto-generated method stub
		List<Object> lists = new ArrayList<>();
		Integer queryStudentCount = mapper.queryStudentCount(data);
		List<Student> queryStudentBy = mapper.queryStudentBy(data);
		lists.add(queryStudentBy);
		lists.add(queryStudentCount);
		return lists;
	}	
	
	
}
