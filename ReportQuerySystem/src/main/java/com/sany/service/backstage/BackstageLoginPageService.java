package com.sany.service.backstage;

import com.sany.entity.user;
import com.sany.utils.Result;

public interface BackstageLoginPageService {
	
	public String login(String username,String password);
	
	public Result<user> queryUser(String token);
		
	
}
