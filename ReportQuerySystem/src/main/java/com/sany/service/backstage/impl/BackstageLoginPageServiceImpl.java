package com.sany.service.backstage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sany.dao.backstage.BackstageLoginPageMapper;
import com.sany.entity.user;
import com.sany.service.backstage.BackstageLoginPageService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;

@Service
public class BackstageLoginPageServiceImpl implements BackstageLoginPageService {
	
	@Autowired
	private BackstageLoginPageMapper mapper;
	
	@Override
	public String login(String username,String password) {
		return mapper.queryUserToken(username, password);
	}

	@Override
	public Result<user> queryUser(String token) {
		// TODO Auto-generated method stub
		if(token!=null) {
		user queryUser = mapper.queryUser(token);
		return ResultGenerator.genSuccessResult(queryUser);
		}
		return ResultGenerator.genErrorResult(200, "token过期");
	}
	
	
}
