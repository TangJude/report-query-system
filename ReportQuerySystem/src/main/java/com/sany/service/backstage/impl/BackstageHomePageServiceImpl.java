package com.sany.service.backstage.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sany.dao.backstage.BackstageHomePageMapper;
import com.sany.entity.Teacher;
import com.sany.service.backstage.BackstageHomePageService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryTeacherPageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import com.sany.utils.TokenGenerator;

@Service
@Transactional
public class BackstageHomePageServiceImpl implements BackstageHomePageService{
	
	@Autowired
	private BackstageHomePageMapper mapper;
	@Autowired
	private TokenGenerator generator;
	
	@Override
	public Result<ArrayList<Object>> queryAllTeacher(PageUtils data) {
		// TODO Auto-generated method stub
		ArrayList<Object> arrayList = new ArrayList<>();
		List<Teacher> queryAllTeacher = mapper.queryAllTeacher(data);
		Integer queryAllTeacherCount = mapper.queryAllTeacherCount();
		arrayList.add(queryAllTeacher);
		arrayList.add(queryAllTeacherCount);
		return ResultGenerator.genSuccessResult(arrayList);
	}

	@Override
	public Result<Integer> addAndUpdateTeacher(Teacher data) {
		// TODO Auto-generated method stub
		Teacher queryTeacherByJobNumber = mapper.queryTeacherByJobNumber(data.getJobNumber());
		Integer i = 0;
		if(data.getId()==0){
			if(queryTeacherByJobNumber==null) {
			Teacher queryRankMinimumTerankacher = mapper.queryRankMinimumTeacher();
			if(queryRankMinimumTerankacher==null) {
				Integer queryRankMinimum = mapper.queryRankMinimum();
				if(queryRankMinimum==null) {
					queryRankMinimum=0;
				}
				data.setRanking(queryRankMinimum+1);
			}else {
				data.setRanking(queryRankMinimumTerankacher.getRanking());
			}
			String token = generator.token();
			mapper.addTeacherToken(data.getJobNumber(), token);
			 i = mapper.addTeacher(data);
			}
		}else {
			if(queryTeacherByJobNumber==null ||queryTeacherByJobNumber.getId()==data.getId()) {
			 i = mapper.updateTeacher(data);
			}
		}
		if(i!=0) {
			return ResultGenerator.genSuccessResult(i);
		}
		
		return ResultGenerator.genSuccessResult(0);
	}

	@Override
	public Result<Integer> deleteTeacher(int id) {
		// TODO Auto-generated method stub、
		int rank=1;
		List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
		for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
			mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
			if(j+1!=queryMaxEnrollmentQuantity.size()) {
				if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
					rank++;
				}
			}
		}
		Teacher queryTeacherInformation = mapper.queryTeacherInformation(id);
		Integer deleteTeacherToken = mapper.deleteTeacherToken(queryTeacherInformation.getJobNumber());
		Integer deleteTeacher = mapper.deleteTeacher(id);
		return ResultGenerator.genSuccessResult(deleteTeacher);
		
	}

	@Override
	public Result<Teacher> queryTeacherInformation(Integer id) {
		// TODO Auto-generated method stub
		Teacher teacherInformation = mapper.queryTeacherInformation(id);
		return ResultGenerator.genSuccessResult(teacherInformation);
	}

	@Override
	public Result<List<Object>> queryTeacher(QueryTeacherPageUtils data) {
		// TODO Auto-generated method stub
		List<Object> lists = new ArrayList<>();
		List<Teacher> queryTeacher = mapper.queryTeacher(data);
		Integer queryTeacherCount = mapper.queryTeacherCount(data);
		lists.add(queryTeacher);
		lists.add(queryTeacherCount);
		return ResultGenerator.genSuccessResult(lists);
	}

	@Override
	public Integer importTeacherExcel(List<Teacher> teacher) {
		// TODO Auto-generated method stub
		for (int i = 0; i < teacher.size(); i++) {
			if(teacher.get(i)!=null) {
			Teacher queryTeacherByJobNumber = mapper.queryTeacherByJobNumber(teacher.get(i).getJobNumber());
			if(queryTeacherByJobNumber==null) {
				if(teacher.get(i).getJobNumber()!=null) {
					Teacher queryRankMinimumTerankacher = mapper.queryRankMinimumTeacher();
					if(queryRankMinimumTerankacher==null) {
						Integer queryRankMinimum = mapper.queryRankMinimum();
						if(queryRankMinimum ==null) {
							queryRankMinimum=0;
						}
						teacher.get(i).setRanking(queryRankMinimum+1);
					}else {
						teacher.get(i).setRanking(queryRankMinimumTerankacher.getRanking());
					}
					String token = generator.token();
					mapper.addTeacherToken(teacher.get(i).getJobNumber(), token);
					mapper.addTeacher(teacher.get(i));
				}
			}else {
				mapper.updateTeacherByjobNumber(teacher.get(i));
			}
		}
			}
		return 1;
	}

	@Override
	public List<Teacher> exportAllTeacher() {
		// TODO Auto-generated method stub
		List<Teacher> queryExportAllTeacher = mapper.queryExportAllTeacher();
		return queryExportAllTeacher;
	}
	
	
	
}
