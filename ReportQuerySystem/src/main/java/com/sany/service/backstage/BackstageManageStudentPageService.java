package com.sany.service.backstage;

import java.util.List;

import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;

public interface BackstageManageStudentPageService {
	
	List<Object> queryAllStudent(PageUtils data);
	
	Integer addAndUpdateStudent(Student data);
	
	Integer deleteStudent(Integer sId);
	
	List<Student> exportStudentExcel();
	
	Integer importStudentExcel(List<Student> data);
	
	//条件查询
	 List<Object> queryStudentBy(QueryStudentPageUtils data);
}
