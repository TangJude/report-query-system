package com.sany.service.clientSide.Impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sany.dao.clientSide.ClientSideHomePageMapper;
import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.service.clientSide.ClientSideHomePageService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;

@Service
@Transactional
public class ClientSideHomePageServiceImpl implements ClientSideHomePageService {

	@Autowired
	private ClientSideHomePageMapper mapper;

	@Override
	public Result<ArrayList<Object>> queryAllStudent(PageUtils data, String token) {
		// TODO Auto-generated method stub
		ArrayList<Object> arrayList = new ArrayList<>();
		List<Student> queryAllStudent = mapper.queryAllStudent(data, token);
		Integer queryAllStudentCount = mapper.queryAllStudentCount(token);
		arrayList.add(queryAllStudent);
		arrayList.add(queryAllStudentCount);
		return ResultGenerator.genSuccessResult(arrayList);
	}

	@Override
	public Result<Integer> addAndUpdateStudent(Student data, String token) {
		// TODO Auto-generated method stub
		Integer i = 0;
		Student queryStudentByIdentityCard = mapper.queryStudentByIdentityCard(data.getPhone());
		if (data.getSId() == 0) {
			if (queryStudentByIdentityCard == null) {
				Teacher queryTeacher = mapper.queryTeacher(token);
				data.setJobNumber(queryTeacher.getJobNumber());
				data.setRecruitTeacher(queryTeacher.getName());
				i = mapper.addStudents(data);
				Integer addEnrollmentQuantity = mapper.addEnrollmentQuantity(queryTeacher.getJobNumber());
				if (addEnrollmentQuantity != 0 || addEnrollmentQuantity != null) {
					int rank = 1;
					List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
					for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
						mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
						if (j + 1 != queryMaxEnrollmentQuantity.size()) {
							if (!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity()
									.equals(queryMaxEnrollmentQuantity.get(j + 1).getEnrollmentQuantity())) {
								rank++;
							}
						}
					}
				}
			}
		} else {
			if (queryStudentByIdentityCard == null || queryStudentByIdentityCard.getSId() == data.getSId()) {
				Teacher queryTeacher = mapper.queryTeacher(token);
				data.setJobNumber(queryTeacher.getJobNumber());
				data.setRecruitTeacher(queryTeacher.getName());
				i = mapper.updateStudent(data);
			}
		}
		return ResultGenerator.genSuccessResult(i);
	}

	@Override
	public Result<Student> queryUpdateStudent(Integer sId) {
		// TODO Auto-generated method stub
		Student queryUpdateStudent = mapper.queryUpdateStudent(sId);
		return ResultGenerator.genSuccessResult(queryUpdateStudent);
	}

	@Override
	public Result<Integer> deleteStudent(Integer sId, String token) {
		// TODO Auto-generated method stub
		Teacher queryTeacher = mapper.queryTeacher(token);
		Integer reduceEnrollmentQuantity = mapper.reduceEnrollmentQuantity(queryTeacher.getJobNumber());
		if (reduceEnrollmentQuantity != 0 || reduceEnrollmentQuantity != null) {
			int rank = 1;
			List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
			for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
				mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
				if (j + 1 != queryMaxEnrollmentQuantity.size()) {
					if (!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity()
							.equals(queryMaxEnrollmentQuantity.get(j + 1).getEnrollmentQuantity())) {
						rank++;
					}
				}
			}
		}
		Integer deleteStudent = mapper.deleteStudent(sId);
		return ResultGenerator.genSuccessResult(deleteStudent);
	}

	@Override
	public Result<List<Object>> queryStudnetInformation(QueryStudentPageUtils data, String token) {
		// TODO Auto-generated method stub
		List<Object> lists = new ArrayList<>();
		List<Student> queryStudnetInformation = mapper.queryStudnetInformation(data, token);
		Integer queryStudentCount = mapper.queryStudentCount(data, token);
		lists.add(queryStudnetInformation);
		lists.add(queryStudentCount);
		return ResultGenerator.genSuccessResult(lists);
	}

	@Override
	public Integer importStudentExcel(List<Student> students) {
		// TODO Auto-generated method stub
		Integer number = 0;
		for (int i = 0; i < students.size(); i++) {
			if (students.get(i) != null) {
				Student queryStudentByIdentityCard = mapper.queryStudentByIdentityCard(students.get(i).getPhone());
				if (queryStudentByIdentityCard == null) {
					number = mapper.addStudents(students.get(i));
					Integer addEnrollmentQuantity = mapper.addEnrollmentQuantity(students.get(i).getJobNumber());
				} else {
					number = mapper.updateStudentByIdentityCard(students.get(i));
				}

			}
		}
		// send notifity
		int rank = 1;
		List<Teacher> queryMaxEnrollmentQuantity = mapper.queryMaxEnrollmentQuantity();
		for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
			mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
			if (j + 1 != queryMaxEnrollmentQuantity.size()) {
				if (!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity()
						.equals(queryMaxEnrollmentQuantity.get(j + 1).getEnrollmentQuantity())) {
					rank++;
				}
			}
		}
		return number;
	}

	@Override
	public List<Student> queryTeacherRecruitAllStudent(String jobNumber) {
		// TODO Auto-generated method stub
		List<Student> queryTeacherRecruitAllStudent = mapper.queryTeacherRecruitAllStudent(jobNumber);
		return queryTeacherRecruitAllStudent;
	}

}
