package com.sany.service.clientSide;

import com.sany.entity.Teacher;
import com.sany.utils.Result;

public interface ClientSideLoginPageService {
	String Login(String jobNumber,String password);
	
	Teacher queryTeacher(String token);
	
	
}
