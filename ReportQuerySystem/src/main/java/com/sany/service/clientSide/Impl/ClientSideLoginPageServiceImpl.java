package com.sany.service.clientSide.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sany.dao.clientSide.ClientSideLoginPageMapper;
import com.sany.entity.Teacher;
import com.sany.service.clientSide.ClientSideLoginPageService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;

@Service
public class ClientSideLoginPageServiceImpl  implements ClientSideLoginPageService{
	
	@Autowired
	private ClientSideLoginPageMapper mapper; 
	
	@Override
	public String Login(String jobNumber, String password) {
		// TODO Auto-generated method stub
		String queryTeacherToken = mapper.queryTeacherToken(jobNumber, password);
		return queryTeacherToken;
	}

	@Override
	public Teacher queryTeacher(String token) {
		// TODO Auto-generated method stub
		Teacher queryTeacher = mapper.queryTeacher(token);
		return queryTeacher;
	}
}
