package com.sany.service.clientSide;

import java.util.ArrayList;
import java.util.List;

import com.sany.entity.Student;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.Result;

public interface ClientSideHomePageService {
	 Result<ArrayList<Object>> queryAllStudent(PageUtils data,String token);
	
	 Result<Integer> addAndUpdateStudent(Student data,String token);
	 
	 Result<Student> queryUpdateStudent(Integer sId);
	 
	 Result<Integer> deleteStudent(Integer sId,String token);
	 
	 Result<List<Object>> queryStudnetInformation(QueryStudentPageUtils data,String token);
	 
	 Integer importStudentExcel(List<Student> students);
	 
	 List<Student> queryTeacherRecruitAllStudent(String jobNumber);
	 
	 
}
