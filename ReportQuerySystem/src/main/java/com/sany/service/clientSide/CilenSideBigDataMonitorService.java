package com.sany.service.clientSide;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.sany.entity.Day;
import com.sany.entity.Month;

public interface CilenSideBigDataMonitorService {
		//我的月份招生数量趋势
		List<Month> queryEveryMonthEnrollmentQuantity(String year,String token);
		
		//我的招生数量总数
		Integer queryEnrollmentQuantitySum(String token);
		
		//前5座城市招生数量
		List<Map<String,Integer>> queryTop5CityEnrollmentQuantity(String token);
		
		//查询当前包含今天的前7天招生数量
		List<Day> queryDateEnrollmentQuantity(String token);
		
		//查询上一周的招生数量
		List<Map<String,String>> queryLastWeekEnrollmentQuantity(String token);
		
		//查询考生关注点
		Map<String,Integer> queryExamineeAttention(String token);
}
