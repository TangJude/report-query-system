package com.sany.service.recruit;

import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.utils.PageUtils;
import com.sany.utils.Result;

import java.util.List;

public interface RecruitHomePageService {

    Result<List<Object>> QueryaRecruitStudent(PageUtils data, String token);

    Result<Integer> addStudent(Student student,String token);

    Integer recruitDelete(int sid);

    Result<Student> queryIdStudent(int sid);

    Integer updateStudent(Student student);

    List<Student> exportRecruitStudentExcel(String token);

    public Integer importRecruitStudentExcel(List<Student> students,String token);

}
