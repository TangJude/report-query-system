package com.sany.service.recruit.Impl;

import com.sany.dao.recruit.RecruitLoginPageMapper;
import com.sany.entity.Recruit;
import com.sany.service.recruit.RecruitLoginPageService;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class RecruitLoginPageServiceImpl implements RecruitLoginPageService {

    @Autowired
    RecruitLoginPageMapper mapper;

    @Override
    public Result Login(String invitationCode, String password) {
        Object token = mapper.queryRecruitToken(invitationCode,password);
        if (token==null){
            return ResultGenerator.genFailResult("邀请码或密码错误");
        }
        return ResultGenerator.genSuccessResult(token);
    }

    @Override
    public Recruit queryRecruit(String token) {
        Recruit recruit = mapper.queryRecruit(token);
        return recruit;
    }
}
