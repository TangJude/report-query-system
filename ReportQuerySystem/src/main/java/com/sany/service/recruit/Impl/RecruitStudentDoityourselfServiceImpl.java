package com.sany.service.recruit.Impl;

import com.sany.dao.recruit.RecruitStudentDoityourselfMapper;
import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.entity.newobject;
import com.sany.service.recruit.RecruitStudentDoityourselfService;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.selectObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecruitStudentDoityourselfServiceImpl implements RecruitStudentDoityourselfService {

    @Autowired
    private RecruitStudentDoityourselfMapper Mapper;

    @Override
    public List<Student> QuerytoOut(String token) {
        return Mapper.selectAllStudent(token);
    }

    public newobject selectAllInternalStudentsPart(String token, QueryStudentPageUtils data) {
        newobject newobject = new newobject();
        List<Student> recruits = Mapper.selectAllInternalStudentsPart(token,data);
        Integer recruits1 = Mapper.selectAllInternalStudentsPartNumber(token,data);
        newobject.setObject(recruits);
        newobject.setNumber(recruits1);
        return newobject;
    }

    @Override
    public newobject teacherqueryexternal(String token,QueryStudentPageUtils data) {
        newobject newobject = new newobject();

        List<Student> teacherqueryexternal = Mapper.teacherqueryexternal(token,data);
        Integer integer = Mapper.teacherqueryexternalNumber(token,data);
        newobject.setObject(teacherqueryexternal);
        newobject.setNumber(integer);
        return newobject;
    }

    public newobject teacherquerypartexternal(String token,QueryStudentPageUtils data) {
        newobject newobject = new newobject();
        List<Recruit> teacherquerypartexternal = Mapper.teacherquerypartexternal(token,data);
        Integer integer = Mapper.teacherquerypartexternalNumber(token,data);
        newobject.setNumber(integer);
        newobject.setObject(teacherquerypartexternal);
        return newobject;
    }

}
