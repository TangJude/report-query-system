package com.sany.service.recruit.Impl;

import com.sany.dao.recruit.RecruitHomePageMapper;
import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.service.recruit.RecruitHomePageService;
import com.sany.utils.PageUtils;
import com.sany.utils.Result;
import com.sany.utils.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RecruitHomePageServiceImpl implements RecruitHomePageService {

    @Autowired
    RecruitHomePageMapper mapper;

    @Override
    public Result<Integer> addStudent(Student student, String token) {
        //查询手机号是否存在
        Student student1 = mapper.queryStudentByIdentityCard(student.getPhone());
        if (student1 == null){
            //根据token得到邀请码
            String invitationCode = mapper.queryInformation(token);
            //根据邀请码得到招生学生信息
            Recruit recruit = mapper.queryRecruit(invitationCode);
            //根据教师工号得到老师信息
            Teacher teacher = mapper.queryTeacher(recruit.getJobNumber());
            student.setJobNumber(teacher.getJobNumber());
            student.setRecruitTeacher(teacher.getName());
            student.setInvitationCode(invitationCode);
            //添加学生
            Integer integer = mapper.addStudent(student);
            //老师招生数加1
            Integer addEnrollmentQuantity = mapper.TeacherEnrollmentQuantity(teacher.getJobNumber());
            //学生招生数加1
            Integer integer1 = mapper.RecruitEnrollmentQuantity(invitationCode);

            //更新教师排名
            if(addEnrollmentQuantity==1 && integer1==1) {
                int rank=1;
                List<Teacher> queryMaxEnrollmentQuantity = mapper.querySortTeacher();
                for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
                    mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
                    if(j+1!=queryMaxEnrollmentQuantity.size()) {
                        if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
                            rank++;
                        }
                    }
                }
            }
            return ResultGenerator.genSuccessResult(integer);
        }else {
            return ResultGenerator.genSuccessResult("手机号已存在");
        }

    }

    @Override
    public Integer recruitDelete(int sid) {
            Student student = mapper.queryIdStudent(sid);
            if (student.getInvitationCode()!=null){
                Integer integer = mapper.RecruitDeleteEnrollment(student.getInvitationCode());
            }
            mapper.TeacherDeleteEnrollment(student.getJobNumber());
            //更新教师排名
            int rank=1;
            List<Teacher> queryMaxEnrollmentQuantity = mapper.querySortTeacher();
            for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
                mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
                if(j+1!=queryMaxEnrollmentQuantity.size()) {
                    if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
                        rank++;
                    }
                }
            }

        Integer integer = mapper.deleteStudent(sid);
        return integer;
    }

    @Override
    public Result<Student> queryIdStudent(int sid) {
        Student student = mapper.queryIdStudent(sid);
        return ResultGenerator.genSuccessResult(student);
    }

    @Override
    public Result<List<Object>> QueryaRecruitStudent(PageUtils data, String token) {
        List<Object> list = new ArrayList<>();
        String invitationCode = mapper.queryInformation(token);

        List<Student> students = mapper.queryStudent(data, invitationCode);
        Integer integer = mapper.queryStudentCount(invitationCode);

        list.add(students);
        list.add(integer);
        return ResultGenerator.genSuccessResult(list);
    }

    @Override
    public Integer updateStudent(Student student) {
        Integer integer = mapper.updateStudent(student);
        return integer;
    }

    @Override
    public List<Student> exportRecruitStudentExcel(String token) {
        String invitationCode = mapper.queryInformation(token);
        List<Student> students = mapper.queryRecruitStudent(invitationCode);
        return students;
    }

    @Override
    public Integer importRecruitStudentExcel(List<Student> students,String token) {
        String s = mapper.queryInformation(token);
        Recruit recruit = mapper.queryRecruit(s);
        Teacher teacher = mapper.queryTeacher(recruit.getJobNumber());
        // TODO Auto-generated method stub
        students.forEach(time->{
            if(time.getSName()!=null) {
                Student queryStudentByIdentityCard = mapper.queryStudentByIdentityCard(time.getPhone());
                time.setInvitationCode(s);
                time.setJobNumber(recruit.getJobNumber());
                time.setRecruitTeacher(teacher.getName());
                if(queryStudentByIdentityCard==null) {
                    mapper.addStudent(time);
                    mapper.TeacherEnrollmentQuantity(time.getJobNumber());
                    mapper.RecruitEnrollmentQuantity(time.getInvitationCode());
                }else {
                    mapper.updateStudentByIdentityCard(time);
                }
            }
        });
        int rank=1;
        List<Teacher> queryMaxEnrollmentQuantity = mapper.querySortTeacher();
        for (int j = 0; j < queryMaxEnrollmentQuantity.size(); j++) {
            mapper.updateTeacherRank(rank, queryMaxEnrollmentQuantity.get(j).getJobNumber());
            if(j+1!=queryMaxEnrollmentQuantity.size()) {
                if(!queryMaxEnrollmentQuantity.get(j).getEnrollmentQuantity().equals(queryMaxEnrollmentQuantity.get(j+1).getEnrollmentQuantity())) {
                    rank++;
                }
            }
        }
        return 1;
    }
}
