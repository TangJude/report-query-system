package com.sany.service.recruit;


import com.sany.entity.Recruit;
import com.sany.utils.Result;

public interface RecruitLoginPageService {
    Result Login(String invitationCode, String password);

    Recruit queryRecruit(String token);
}
