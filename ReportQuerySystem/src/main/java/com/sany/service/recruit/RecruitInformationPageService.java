package com.sany.service.recruit;

import com.sany.entity.Recruit;
import com.sany.utils.PageUtils;
import com.sany.utils.Result;

import java.util.List;


public interface RecruitInformationPageService {

    List<Object> queryAllRecruitVo(PageUtils data,String token);

    public Result addInformation(String token, Recruit recruit);

    public Result<List<Object>> queryTeacherInvitation(PageUtils data,String token);

    public Result<Integer> deleteInvitation(int rid);

    String randomInformation(String token);

    Result<Recruit> queryIdRecruit(Integer id);

    Result updateRecruit(Recruit recruit);

    List<Recruit> exportRecruitExcel(String jobNumber);

    Integer importRecruitExcel(List<Recruit> recruit,String token);


}
