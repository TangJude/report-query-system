package com.sany.service.recruit;

import com.sany.entity.Student;
import com.sany.entity.newobject;
import com.sany.utils.PageUtils;
import com.sany.utils.QueryStudentPageUtils;
import com.sany.utils.selectObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecruitStudentDoityourselfService {


    //导出
    List<Student> QuerytoOut(String token);

    newobject selectAllInternalStudentsPart(String token, QueryStudentPageUtils data);

    newobject teacherqueryexternal(String token,QueryStudentPageUtils data);

    //老师查询内部学生
    newobject teacherquerypartexternal(String token,QueryStudentPageUtils data);

}
