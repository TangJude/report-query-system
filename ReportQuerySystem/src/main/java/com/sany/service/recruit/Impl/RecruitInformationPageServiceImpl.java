package com.sany.service.recruit.Impl;

import com.sany.dao.recruit.RecruitInformationPageMapper;
import com.sany.entity.Recruit;
import com.sany.entity.Student;
import com.sany.entity.Teacher;
import com.sany.service.recruit.RecruitInformationPageService;
import com.sany.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RecruitInformationPageServiceImpl implements RecruitInformationPageService {

    @Autowired
    private RecruitInformationPageMapper mapper;

    @Autowired
    private TokenGenerator generator;

    @Override
    public List<Object> queryAllRecruitVo(PageUtils data,String token) {
        // TODO Auto-generated method stub
        List<Object> lists = new ArrayList<>();
        Teacher teacher = mapper.queryTeacher(token);

        List<Student> queryAllRecruit = mapper.queryAllRecruit(teacher.getJobNumber(),data);
        Integer queryAllRecruitVoCount = mapper.queryAllRecruitCount(teacher.getJobNumber());
        lists.add(queryAllRecruit);
        lists.add(queryAllRecruitVoCount);
        return lists;
    }

    @Override
    public Result addInformation(String token, Recruit recruit){
        Teacher teacher = mapper.queryTeacher(token);
        if (teacher!=null){
            String information = recruit.getInvitationCode();
            if (information==null){
                return ResultGenerator.genFailResult("请输入邀请码");
            }
                //查询邀请码是否存在
                Recruit recruitVo = mapper.queryInformation(information);
                if (recruitVo==null){
                    recruit.setJobNumber(teacher.getJobNumber());
                    Integer integer = mapper.addInformation(recruit);
                    //生成邀请码后添加token
                    String t = generator.token();
                    mapper.addInformationToken(information, t);
                    return ResultGenerator.genSuccessResult(integer);
                }else {
                    return ResultGenerator.genFailResult("邀请码已存在");
                }
            }
        return ResultGenerator.genFailResult("token已过期");
    }

    @Override
    public Result<List<Object>> queryTeacherInvitation(PageUtils data,String token) {
        List<Object> list = new ArrayList<>();
        Teacher teacher = mapper.queryTeacher(token);
        if (teacher!=null){
            List<Recruit> recruits = mapper.queryTeacherInformation(teacher.getJobNumber(),data);
            Integer integer = mapper.queryTeacherInformationCount(teacher.getJobNumber());
            list.add(recruits);
            list.add(integer);
            return ResultGenerator.genSuccessResult(list);
        }
        return ResultGenerator.genFailResult("token已过期");
    }

    @Override
    public Result<Integer> deleteInvitation(int rid) {
            //根据id查询邀请码信息
            Recruit recruit = mapper.queryIdInformation(rid);
            String invitationCode = recruit.getInvitationCode();
            //清除token
            mapper.deleteInformationToken(invitationCode);
            //删除邀请码
            mapper.deleteInformation(rid);
            return ResultGenerator.genSuccessResult();

    }

    @Override
    //生成随机邀请码
    public String randomInformation(String token) {
        Teacher teacher = mapper.queryTeacher(token);
        if (teacher!=null){
            boolean bool = true;
            do {
                String invitation = GenerateInvitation.getInvitation();
                Recruit recruit = mapper.queryInformation(invitation);
                if (recruit ==null){
                    return invitation;
                }
            }while (bool);
        }
            return "token已过期";
    }

    @Override
    public Result<Recruit> queryIdRecruit(Integer id) {
        Recruit recruit = mapper.queryIdInformation(id);
        return ResultGenerator.genSuccessResult(recruit);
    }

    @Override
    public Result updateRecruit(Recruit recruit) {
        Integer integer = mapper.updateRecruit(recruit);
        if (integer == 1) {
            return ResultGenerator.genSuccessResult(integer);
        }
        return ResultGenerator.genFailResult("");
    }

    @Override
    public List<Recruit> exportRecruitExcel(String jobNumber) {
        List<Recruit> recruits = mapper.queryjobNumber(jobNumber);
        return recruits;
    }

    @Override
    public Integer importRecruitExcel(List<Recruit> recruit,String token) {
        Teacher teacher = mapper.queryTeacher(token);
        recruit.forEach(time-> {
            if (time.getInvitationCode() == null) {
                boolean bool = true;
                while (bool) {
                    String invitation = GenerateInvitation.getInvitation();
                    Recruit recruit1 = mapper.queryInformation(invitation);
                    if (recruit1 == null) {
                        time.setInvitationCode(invitation);
                        String token1 = generator.token();
                        mapper.addInformationToken(invitation,token1);
                        bool = false;
                    }
                }
                time.setJobNumber(teacher.getJobNumber());
                mapper.addInformation(time);
            }
        });

        return 1;
    }
}
