package com.sany.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

@Configuration
@EnableOpenApi
public class SwaggerConfig {
	@Value("${swagger.enable:true}")
	private boolean enable;
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(enable)
                .groupName("lazn")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sany.controller"))
                .build();
    }

    public ApiInfo apiInfo(){
        Contact contact = new Contact("橘子来了研发一组", "https://blog.csdn.net/pdj0217", "2300421772@qq.com");
        return new ApiInfo(
               "单招报备查询系统接口文档",
               "橘子来了研发一组的接口地址",
               "vip999999",
               "https://blog.csdn.net/pdj0217",
                contact,
               "Apache 2.0",
               "http://www.apache.org/licenses/LICENSE-2.0",
               new ArrayList());

    }
}
