package com.sany.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInterceptor;


public class Snippet {
	@Bean(name = "businessSqlSessionFactory")
	    public SqlSessionFactory businessSqlSessionFactory(@Qualifier("businessDataSource") DataSource dataSource) throws Exception {
	        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
	        factoryBean.setDataSource(dataSource);
	        factoryBean.setTypeAliasesPackage("org.safety.manager.entity");
	        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/business/*.xml"));
	        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
	        configuration.setLogImpl(StdOutImpl.class);
	        configuration.setMapUnderscoreToCamelCase(true);
	        configuration.setCallSettersOnNulls(true);
	        factoryBean.setConfiguration(configuration);
	        //分页插件
	        Interceptor interceptor = new PageInterceptor();
	        Properties properties = new Properties();
	        //数据库
	        properties.setProperty("helperDialect", "mysql");
	        //是否将参数offset作为PageNum使用
	        properties.setProperty("offsetAsPageNum", "true");
	        //是否进行count查询
	        properties.setProperty("rowBoundsWithCount", "true");
	        //是否分页合理化
	        properties.setProperty("reasonable", "false");
	        interceptor.setProperties(properties);
	        factoryBean.setPlugins(new Interceptor[] {interceptor});
	        return factoryBean.getObject();
	    }
}

