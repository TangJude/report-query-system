package com.sany.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Address implements Serializable{
	@Excel(name = "省份",width = 12)
	private String province;
	
	@Excel(name = "市区",width = 12)
	private String urbanDistrict;
	
	@Excel(name = "县城",width = 12)
	private String countyTown;
	
	@Excel(name ="详细地址",width = 20)
	private String detailedAddress;
}
