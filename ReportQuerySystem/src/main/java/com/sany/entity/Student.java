package com.sany.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel("学生信息类")
@ExcelTarget("student")
public class Student implements Serializable {
	@ExcelIgnore
	private int sId;
	
	@Excel(name = "姓名",width = 8)
	private String sName;
	
	@Excel(name = "性别", replace = {"男_1","女_2"},width = 5)
	private Integer sex;

	@Excel(name = "手机号",width = 15)
	private String phone;
	
	@Excel(name = "身份证",width = 27)
	private String identityCard;
	
	@ExcelCollection(name ="生源所在地")
	private List<Address> address;
	
	@Excel(name = "毕业学校",width = 20)
	private String graduationSchool;
	
	@Excel(name = "考生关注点",width = 15)
	private String examineeAttention;
	
	@Excel(name = "招生老师",width = 12)
	private String recruitTeacher;
	
	@Excel(name = "教师工号",width = 15)
	private String jobNumber;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
	@Excel(name ="对接日期", format = "yyyy-MM-dd",width = 17)
	private Date dockingTime;

	@Excel(name = "邀请码",width = 15)
	private String invitationCode;

	//备注
	@Excel(name = "备注",width = 15)
	private String remarks;
	
}
