package com.sany.entity;

import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@ApiModel("月份实体类")
public class Month {
	@ApiModelProperty("月份")
	private Integer month;
	@ApiModelProperty("数量")
	private Integer count;
}
