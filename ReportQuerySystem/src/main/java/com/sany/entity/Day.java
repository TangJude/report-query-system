package com.sany.entity;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Day {
	private Integer day;
	private String count;
	
	public String getDay() {
		return this.day+"日";
	}
}
