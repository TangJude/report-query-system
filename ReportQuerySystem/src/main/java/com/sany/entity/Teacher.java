package com.sany.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel("老师信息类")
@ExcelTarget("teacher")
public class Teacher {
	
	@ExcelIgnore
	private int id;
	
	@Excel(name ="教师姓名")
	private String name;
	
	@Excel(name ="性别",replace = {"男_1","女_2"},width = 5)
	private Integer sex;
	
	@Excel(name = "教师工号")
	private String jobNumber;
	
	@ExcelIgnore
	private String password;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelIgnore
	private Date submitTime;
	
	@Excel(name ="手机号",width = 15)
	private String phone;
	
	@Excel(name= "招生数量")
	private Integer enrollmentQuantity;
	
	@Excel(name="排名")
	private Integer ranking;
}
