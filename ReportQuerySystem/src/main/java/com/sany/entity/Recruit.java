package com.sany.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel("招生学生信息")
@ExcelTarget("recruit")
public class Recruit {
    @ExcelIgnore
    private int rId;

    @Excel(name = "邀请码",width = 15)
    private String invitationCode;

    @Excel(name = "招生学生姓名",width =15)
    private String rName;

    @Excel(name = "教师工号",width = 15)
    private String jobNumber;

    @Excel(name = "性别", replace = {"男_1","女_2"},width = 5)
    private Integer sex;

    @ExcelCollection(name ="地区")
    private List<Address> address;

    @Excel(name = "身份证",width = 27)
    private String idCard;

    @Excel(name = "手机号",width = 15)
    private String phone;

    @Excel(name = "密码",width = 12)
    private String passWord;

    @Excel(name = "招生数量")
    private Integer enrollmentQquantity;


}
