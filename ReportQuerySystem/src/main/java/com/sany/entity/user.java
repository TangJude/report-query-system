package com.sany.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel("用户类")
public class user {
	private String name;
	
	private Integer sex;
	
	private String userName;
	
	private String power;
	
}
