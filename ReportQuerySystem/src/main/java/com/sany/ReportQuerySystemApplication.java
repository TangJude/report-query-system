package com.sany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReportQuerySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReportQuerySystemApplication.class, args);
	}

}
